﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToolScripts;
using UnityEngine.SceneManagement;
using TMPro;


public class CircleType
{
    public float x, y, r;
    public CircleType(float x,float y,float r)
    {
        this.x = x;
        this.y = y;
        this.r = r;
    }
}

/* [System.Serializable]
public class RectType
{
    public float x, y, width, height;
    public RectType(float x,float y,float width,float height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}*/


public class test : MonoBehaviour
{
    public SpriteRenderer brick_breaker_bar;
    public SpriteRenderer ball;
    
    //boy*(en/boy)
    //aspect_ratio*camera.size  //düzelt
    
    public Camera camera;
    
    public GameObject[] walls;
    public List<GameObject> bricks=new List<GameObject>();
    
    public List<Rect> borders=new List<Rect>(); //sınırlar
    public List<Rect> brick = new List<Rect>();
    public List<Rect> prizeRectList = new List<Rect>();

    protected CircleType circle;
    protected Rect bar_rect;

    public List<GameObject> prizeList = new List<GameObject>();

    protected Rect rect;
    CircleType player;
    Rect bar;

    public TextMeshProUGUI scoreText;
    int score = 0;

    public float speed = 2f;
    Vector2 direction_ball=new Vector2(2f,3f);

    float timer = 50f;
    float degisken;
    float wall_0;
    float wall_1;

    Vector3 prizeFallPos;

    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "Score: " + score;

        foreach (var item in walls) //sınırlar
        {
            Rect border = item.GetBoundingBox();// new RectType(item.transform.position.x,item.transform.position.y,item.transform.lossyScale.x, item.transform.lossyScale.y);
            borders.Add(border);
        }
        
        TouchManager.Instance.onTouchMoved += TouchMoved;

        foreach (var item in bricks)
        {
            Rect brick_break = item.GetBoundingBox();//new Rect(item.transform.position.x,item.transform.position.y, item.transform.lossyScale.x, item.transform.lossyScale.y);
            brick.Add(brick_break);
           // Debug.Log("tuğlalar eklendi");
            Debug.Log(brick);
        }

        foreach (var item in prizeList)
        {
            Rect prizes = item.GetBoundingBox();
            prizeRectList.Add(prizes);
        }
    }

    private void Update()
    {
        bool isFall=false;
        normalizeVector(direction_ball);
        direction_ball *= speed;
        player = new CircleType(ball.transform.position.x,ball.transform.position.y,ball.transform.lossyScale.y/2);
        bar = brick_breaker_bar.gameObject.GetBoundingBox();// new Rect(brick_breaker_bar.transform.position.x, brick_breaker_bar.transform.position.y, brick_breaker_bar.transform.lossyScale.x, brick_breaker_bar.transform.lossyScale.y);

        Vector2 normal = Vector2.zero;
        foreach (var item in borders)
        {
            if (collisionDetection(player, item,ref normal))
            {
                //Debug.Log("collision");
                crashEdge(direction_ball,normal);
                break;
            }
        }
        if (collisionDetection(player,bar,ref normal))
        {
            crashEdge(direction_ball, normal);
        }

         for (int i=0;i<brick.Count;i++)
         {
            //print("üst"+direction_ball);
            if (collisionDetection(player,brick[i],ref normal))
             {
                crashEdge(direction_ball,normal);
                brick.Remove(brick[i]);

                Debug.Log(bricks[i]);

                GameObject destroyBrick = bricks[i];
                
                prizeFallPos = destroyBrick.transform.position;
                bricks.Remove(destroyBrick);
                Destroy(destroyBrick);
                isFall = true;

                score +=5;
                scoreText.text = "Score: " + score;

                if (brick.Count<=0)
                {
                    Debug.Log("oyun bitti");

                    while (timer> 0)
                    {
                        timer -= Time.deltaTime;
                        Debug.Log(" "+timer);
                    } 

                    if (timer<=0)
                    {
                        SceneManager.LoadScene("brick_breaker");
                    }
                }
             }
        }
        ball.transform.position += (Vector3)direction_ball * Time.deltaTime;
        for (int i = 0; i < prizeRectList.Count; i++)
        {
            Debug.Log("PRİZE  RECT LISTT"+prizeRectList[i]);
            if (collisionDetectionPrize(bar, prizeRectList[i]))
            {
                Debug.Log("ball & prize collision");
                Debug.Log("prizze'ı destroy et");
            }
        }

        if (isFall)
        {
            prizeFall(prizeFallPos);
        }


    }

    private void TouchMoved(TouchInput touchInput)
    {
        Vector3 position_x = new Vector3(touchInput.DeltaWorldPosition.x, 0, 0);
        float leftWallLlimit = walls[1].transform.position.x + (walls[1].transform.position.x / 100);
        float rightWallLimit = walls[0].transform.position.x + (walls[0].transform.position.x / 100);

        if ((brick_breaker_bar.transform.position.x + (brick_breaker_bar.transform.position.x/2 )) >=leftWallLlimit  && 
           ( brick_breaker_bar.transform.position.x+ brick_breaker_bar.transform.position.x/2) <=rightWallLimit)
        {
            brick_breaker_bar.transform.position += position_x;
        }
        else
        {
            if (brick_breaker_bar.transform.position.x>0)
            {
                brick_breaker_bar.transform.position += new Vector3(-0.01f, 0, 0);
            }
            else
            {
                brick_breaker_bar.transform.position += new Vector3(0.01f, 0, 0);
            }
        }
    }


    private void crashEdge(Vector2 directionball,Vector2 normal)
    {
        /* Vector2 directionBallEdge = new Vector2(directionball.x,directionball.y);
         Vector3 direction = (Vector3)directionBallEdge * Time.deltaTime;
         ball.transform.position = Vector3.Reflect(direction,Vector3.right);*/
        // Debug.Log(ball.transform.position);

        direction_ball = Vector2.Reflect(directionball,normal);
    }

    
    bool collisionDetection(CircleType circle, Rect rect, ref Vector2 normal)
    {
        float NearestX = Mathf.Clamp(circle.x, rect.x - rect.width / 2, rect.x + rect.width / 2);
        float NearestY = Mathf.Clamp(circle.y, rect.y - rect.height / 2, rect.y + rect.height / 2);

        Vector2 closestPoint = new Vector2(NearestX, NearestY);
        Vector2 circleCenter = new Vector2(circle.x, circle.y);
        Vector2 dp = closestPoint - circleCenter;

        normal = dp / dp.magnitude; ;
        normal *= -1;

        float dist = dp.magnitude;
        bool collided = (dist) <= (circle.r);
        return collided;
    }

    public void normalizeVector(Vector2 directionBall)
    {
        float length = Mathf.Sqrt((directionBall.x*directionBall.x)+(directionBall.y*directionBall.y));
        float normalizeX = directionBall.x / length;
        float normalizeY = directionBall.y / length;
        Vector2 normalizedDirection = new Vector2(normalizeX,normalizeY);
        //Debug.Log("Nomralized vector:"+normalizedDirection);
        direction_ball = normalizedDirection;
    }

    public void prizeFall(Vector3 prizeFallPos)
    {
            System.Random rnd = new System.Random();
            int num=rnd.Next(0,9);
            prizeList[num].transform.position = prizeFallPos;
            Debug.Log("przie list eleman:" + prizeList[num]);
            Debug.Log("prize fallfonks içinde");

            if (prizeList[num].transform.position.y >= walls[3].transform.position.y)
            {
                Debug.Log("fonksiton y içinde");
                Vector3 prizeY = new Vector3(0, -5f, 0);
                Debug.Log("prizelİST POS BEFORE" + prizeList[num].transform.position);
                prizeList[num].transform.position += prizeY*Time.deltaTime;
                Debug.Log("prizelİST POS AFTER:"+prizeList[num].transform.position);
            }
        }
       
    bool collisionDetectionPrize(Rect rect1,Rect rect2)
    {
        float rect1Left = rect1.x - rect1.width / 2;
        float rect2Right = rect2.x + rect2.width / 2;
        float rect1Right = rect1.x + rect1.width / 2;
        float rect2Left = rect2.x - rect2.width / 2;
        float rect1Top = rect1.y + rect1.height / 2;
        float rect2Top = rect2.y + rect2.height / 2;
        float rect1Bottom = rect1.y - rect1.height / 2;
        float rect2Bottom = rect2.y - rect2.height / 2; ;


        if (rect1Left<rect2Right && rect1Right>rect2Left && rect1Top<rect2Bottom && rect1Bottom>rect2Top)
        {
            Debug.Log("collision DETECTION PRIZE");
            return true;
        }
        else
        {
            return false;
        }

    }  


}
